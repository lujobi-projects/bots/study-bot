FROM python:latest

WORKDIR /app
COPY . /app

VOLUME /data

RUN pip install -r requirements.txt

CMD ["python", "-u", "src/utils/command_info.py", "&&", "python", "-u", "bot.py"]
