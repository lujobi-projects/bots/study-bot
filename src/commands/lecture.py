import json
from os import getcwd, getenv
from os.path import abspath, join

from ..utils.message import date_to_string

config_file = abspath(getenv('CONFIG', '/config.json'))
cfg = json.load(open(config_file))


def info(lec):
  text = ''
  for info_key, info_value in lec.get('info', []).items():
    if type(info_value) == dict and info_value.get('link', False):
      text += f'{info_key}: [link]({info_value["link"]}) \n'
    else:
      text += f'{info_key}: {info_value} \n'

  lessons = lec.get('lessons', [])
  if len(lessons) > 0:
    text += f'_Lessons_: \n'
    for value in lessons:
      text += f'     \* {date_to_string(value)}\n'

  exercises_sessions = lec.get('exercises_sessions', [])
  if len(exercises_sessions) > 0:
    text += f'_Exercise Sessions_: \n'
    for value in exercises_sessions:
      text += f'     \* {date_to_string(value)}\n'

  return text
