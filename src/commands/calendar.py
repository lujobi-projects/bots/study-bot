import json
from os import getcwd, getenv
from os.path import abspath, join

from ..utils.message import DAY_MAP


data_file = abspath(getenv('DATA', '/data/data.json'))
config_file = abspath(getenv('CONFIG', '/config.json'))
with open(config_file) as f:
  cfg = json.load(f)

calendar = [[[] for _ in range(24)]
            for _ in range(7)]

for lec_key, lec in cfg['lectures'].items():
  for lesson in lec.get('lessons', []):
    day_ind = lesson[0]-1
    hour_ind = lesson[1][0]-1
    hours = lesson[1][1] - lesson[1][0]
    for i in range(hours):
      calendar[day_ind][hour_ind + i].append(lec_key)

  for exercise in lec.get('exercises_sessions', []):
    day_ind = exercise[0]-1
    hour_ind = exercise[1][0]-1
    hours = exercise[1][1] - exercise[1][0]
    for i in range(hours):
      calendar[day_ind][hour_ind + i].append('ex_' + lec_key)


def calendar_day(day):
  res = f'{DAY_MAP[day]}\: \n'
  for i, hour in enumerate(calendar[day-1]):
    if hour != []:
      hour_string = f'     _\[{i+1}\.00 \- {i+2}\.00\]_\: '
      for lesson in hour:
        ex = '\(Ex\) ' if lesson.startswith('ex_') else ''
        cmd = '\/' + (lesson if not lesson.startswith('ex_') else lesson[3:])
        hour_string += f'{ex}{cmd} '
      res += hour_string + '\n'
  return res


def complete_calendar():
  res = f'*Calendar for {cfg["meta"]["user"]}* \n'
  for day in range(7):
    res += calendar_day((day+1) % 7) + '\n'
  return res
