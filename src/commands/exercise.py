import json
from os import getcwd, getenv
from os.path import abspath, join


data_file = abspath(getenv('DATA', '/data/data.json'))
config_file = abspath(getenv('CONFIG', '/config.json'))
with open(config_file) as f:
  cfg = json.load(f)


def format_array(arr):
  if arr == []:
    return '\[\]'
  text = '\['
  start = arr[0]
  curr = start
  res = []
  for i in range(1, len(arr)):
    if curr + 1 != arr[i]:
      text += f'{start}\-{curr}, ' if start != curr else f'{start}, '
      start = arr[i]
    curr = arr[i]

  text += f'{start}\-{curr}\]' if start != curr else f'{start}\]'

  return text


def info(key, lec):
  with open(data_file) as f:
    data = json.load(f)

  text = ''
  ctr = 1
  for ex_key, count in lec.get('exercises', {}).items():
    intro = f'_{ctr} {ex_key}_ \(last dist\. ex {data["dist"][key][ex_key]}\): '

    text_done = '     done: \['
    text_open = '     *open*: \['
    text_marked = '     _marked_: \['
    ctr += 1
    for i in range(1, count + 1):
      if i in data['marked'][key][ex_key]:
        text_marked += f'{i}, '
      if i in data['done'][key][ex_key]:
        text_done += f'{i}, '
      else:
        if i <= data['dist'][key][ex_key]:
          text_open += f'*{i}*, '
        else:
          text_open += f'{i}, '
    if len(text_done) > 13:
      text_done = text_done[:-2]
    text_done += '\]'
    if len(text_open) > 15:
      text_open = text_open[:-2]
    text_open += '\]'
    if len(text_marked) > 17:
      text_marked = text_marked[:-2]
    text_marked += '\]'
    text += f'{intro}\n{text_open}\n{text_marked}\n{text_done}\n'
  return text


def undo(key, lec, cat, no):
  with open(data_file) as f:
    data = json.load(f)

  cat_name = ''
  counter = 0
  cat -= 1

  if not (0 <= cat < len(data['done'][key].keys())):
    return 'wrong category'

  for cat_name_done, item in data['done'][key].items():
    if counter == cat and no in item:
      if not (0 <= no < lec['exercises'][cat_name_done]):
        return 'error in key'
      if no in item:
        item.remove(no)
        cat_name = cat_name_done
        break
      else:
        return f'_Exercise not marked as done yet_ \n {info(key, lec)}'
    counter += 1

  with open(data_file, 'w') as f:
    json.dump(data, f)

  return f'_Removed \[{no}\] from Category_ {cat_name}\n {info(key, lec)}'


def unmark(key, lec, cat, no):
  with open(data_file) as f:
    data = json.load(f)

  cat_name = ''
  counter = 0
  cat -= 1

  if not (0 <= cat < len(data['marked'][key].keys())):
    return 'wrong category'

  for cat_name_done, item in data['marked'][key].items():
    if counter == cat and no in item:
      if not (0 <= no < lec['exercises'][cat_name_done]):
        return 'error in key'
      if no in item:
        item.remove(no)
        cat_name = cat_name_done
        break
      else:
        return f'_Exercise not marked yet_ \n {info(key, lec)}'
    counter += 1

  with open(data_file, 'w') as f:
    json.dump(data, f)

  return f'_Removed Mark \[{no}\] from Category_ {cat_name}\n {info(key, lec)}'


def do(key, lec, cat, no):
  with open(data_file) as f:
    data = json.load(f)

  cat_name = ''
  ct = 0
  cat -= 1

  if not (0 <= cat < len(data['done'][key].keys())):
    return 'wrong category'

  for cat_name_done, item in data['done'][key].items():
    if ct == cat:
      if not (0 <= no < lec['exercises'][cat_name_done]):
        return 'error in key'
      if no not in item:
        item.append(no)
        cat_name = cat_name_done
        break
      else:
        return f'_Exercise already marked as done_ \n {info(key, lec)}'
    ct += 1

  with open(data_file, 'w') as f:
    json.dump(data, f)

  return f'_Added \[{no}\] from Category_ {cat_name}\n {info(key, lec)}'


def mark(key, lec, cat, no):
  with open(data_file) as f:
    data = json.load(f)

  cat_name = ''
  ct = 0
  cat -= 1

  if not (0 <= cat < len(data['marked'][key].keys())):
    return 'wrong category'

  for cat_name_done, item in data['marked'][key].items():
    if ct == cat:
      if not (0 <= no < lec['exercises'][cat_name_done]):
        return 'error in key'
      if no not in item:
        item.append(no)
        cat_name = cat_name_done
        break
      else:
        return f'_Exercise already marked_ \n {info(key, lec)}'
    ct += 1

  with open(data_file, 'w') as f:
    json.dump(data, f)

  return f'_Marked \[{no}\] from Category_ {cat_name}\n {info(key, lec)}'


def dist(key, lec, cat):
  with open(data_file) as f:
    data = json.load(f)

  cat_name = ''
  ct = 0
  cat -= 1

  if not (0 <= cat < len(data['dist'][key].keys())):
    return 'wrong category'

  no = 0

  for cat_name_done, item in data['dist'][key].items():
    if ct == cat:
      no = data['dist'][key][cat_name_done]
      if no < lec['exercises'][cat_name_done]:
        no += 1
        data['dist'][key][cat_name_done] = no
        cat_name = cat_name_done
        break
      else:
        return f'_All exercises were already marked as distributed_ \n {info(key, lec)}'
    ct += 1

  with open(data_file, 'w') as f:
    json.dump(data, f)

  return f'_Marked \[{no}\] from Category_ {cat_name} as last distributed\n {info(key, lec)}'


def undist(key, lec, cat):
  with open(data_file) as f:
    data = json.load(f)

  cat_name = ''
  ct = 0
  cat -= 1

  if not (0 <= cat < len(data['dist'][key].keys())):
    return 'wrong category'

  no = 0

  for cat_name_done, item in data['dist'][key].items():
    if ct == cat:
      no = data['dist'][key][cat_name_done]
      if no > 0:
        no -= 1
        data['dist'][key][cat_name_done] = no
        cat_name = cat_name_done
        break
      else:
        return f'_No exercises were marked as distributed_ \n {info(key, lec)}'
    ct += 1

  with open(data_file, 'w') as f:
    json.dump(data, f)

  return f'_Marked \[{no}\] from Category_ {cat_name} as last distributed\n {info(key, lec)}'


def summary():
  with open(data_file) as f:
    data = json.load(f)

  text = '*Exercise Summary* \nOpen Exercises: \n'
  for lec_key, lec in cfg['lectures'].items():
    text += f'{lec["full_name"]}:\n'
    ct = 1
    for ex_key, count in lec.get('exercises', {}).items():
      text += f'     _{ct} {ex_key}_: '
      open_ex = [i for i in range(
          1, count + 1) if i not in data['done'].get(lec_key, []).get(ex_key, [])]
      text += format_array(open_ex)
      dist = data["dist"][lec_key][ex_key]
      open_list = list(filter(lambda x: x <= dist, open_ex))
      text += f' of {count}, dist {dist}, *todo {len(open_list)}*, _mrk {len(data["marked"][lec_key][ex_key])}_\n '
      ct += 1
    if not lec.get('exercises', False):
      text += '     _No exercises specified_'
  return text
