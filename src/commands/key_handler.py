from os import getenv
from os.path import abspath
import json

from telepot.namedtuple import InlineKeyboardButton

from ..utils.message import key_to_cmd
from .command_handler import handle

config_file = abspath(getenv('CONFIG', '/config.json'))
data_file = abspath(getenv('DATA', '/data/data.json'))

with open(config_file) as f:
  cfg = json.load(f)

BASIS_KEYS = [
    # InlineKeyboardButton(text='Info', callback_data='info'),
    InlineKeyboardButton(text='Exercises', callback_data='ex'),
    InlineKeyboardButton(text='Calendar', callback_data='calendar'),
    InlineKeyboardButton(text='Overview', callback_data='all'),
    InlineKeyboardButton(text='Lecture', callback_data='lecture'),
]


def group_keys(keys, cols):
  res = [[]]
  tmp = []

  for i in range(len(keys)):
    res[-1].append(keys[i])
    if len(res[-1]) == cols:
      res.append([])

  return res


BASIS_KEYS = group_keys(BASIS_KEYS, 2)

BACK_KEY = InlineKeyboardButton(text='Back to Start', callback_data='back')


def handle_key(msg):

  with open(data_file) as f:
    data_json = json.load(f)

  data = msg['data']
  chat_id = msg['message']['chat']['id']

  splitted = data.split(' ')
  cmds = [key_to_cmd(k) for k in cfg['lectures'].keys()]

  if data == 'back':
    return BASIS_KEYS, f'*Study bot configured for {cfg["meta"]["user"]} \n*'

  elif data in ['info', 'calendar', 'all', 'ex']:
    return BASIS_KEYS, handle('/' + data, chat_id)
  elif data == 'lecture':
    keys = []
    for key, item in cfg['lectures'].items():
      keys.append(InlineKeyboardButton(
          text=item['full_name'], callback_data=key_to_cmd(key)))
    keys.append(BACK_KEY)
    return group_keys(keys, 1), f'*Study bot configured for {cfg["meta"]["user"]} \n*Your Lectures:'
  elif data in cmds:
    res = handle(f'/{data}', chat_id)
    keys = [
        [InlineKeyboardButton(
            text='Exercise', callback_data=f'{data} ex'), ],
        [InlineKeyboardButton(
            text='Exercise do', callback_data=f'{data} ex do'),
         InlineKeyboardButton(text='Exercise undo',
                              callback_data=f'{data} ex undo'), ],
        [InlineKeyboardButton(
            text='Exercise dist', callback_data=f'{data} ex dist'),
         InlineKeyboardButton(text='Exercise undist', callback_data=f'{data} ex undist')],
        [InlineKeyboardButton(
            text='Exercise mark', callback_data=f'{data} ex mark'),
         InlineKeyboardButton(text='Exercise unmark', callback_data=f'{data} ex unmark')],
        [BACK_KEY]
    ]

    return keys, res
  elif len(splitted) == 2 \
          and splitted[1] == 'ex' \
          and splitted[0] in cmds:
    res = handle(f'/{data}', chat_id)
    keys = [
        [InlineKeyboardButton(
            text='Exercise do', callback_data=f'{data} do'),
         InlineKeyboardButton(text='Exercise undo',
                              callback_data=f'{data} undo'), ],
        [InlineKeyboardButton(
            text='Exercise dist', callback_data=f'{data} dist'),
         InlineKeyboardButton(text='Exercise undist', callback_data=f'{data} undist')],
        [InlineKeyboardButton(
            text='Exercise mark', callback_data=f'{data} mark'),
         InlineKeyboardButton(text='Exercise unmark', callback_data=f'{data} unmark')],
        [BACK_KEY]
    ]

    return keys, res
  elif len(splitted) == 3 \
          and splitted[1] == 'ex' \
          and (splitted[2] == 'do' or splitted[2] == 'undo'
               or splitted[2] == 'mark' or splitted[2] == 'unmark'
               or splitted[2] == 'dist' or splitted[2] == 'undist') \
          and splitted[0] in cmds:
    lec = {}

    for k, i in cfg['lectures'].items():
      if key_to_cmd(k) == splitted[0]:
        lec = i
        break

    res = f'Please select category of exercise \({lec["full_name"]}\) to {splitted[2]}'

    keys = []
    for i, e in enumerate(lec.get('exercises', [])):
      keys.append(
          InlineKeyboardButton(
              text=e, callback_data=f'{data} {i+1}'))

    keys.append(BACK_KEY)
    return group_keys(keys, 2), res
  elif len(splitted) == 4 \
          and splitted[1] == 'ex' \
          and (splitted[2] == 'do' or splitted[2] == 'undo'
               or splitted[2] == 'mark' or splitted[2] == 'unmark') \
          and splitted[0] in cmds:
    lec = {}
    lec_key = ''

    for k, i in cfg['lectures'].items():
      if key_to_cmd(k) == splitted[0]:
        lec = i
        lec_key = k
        break

    keys = []
    cat_name = list(lec['exercises'].keys())[int(splitted[3])-1]
    num_ex = lec['exercises'][cat_name]

    res = f'Please select which exercise \({lec["full_name"]}\) in category {cat_name} to {splitted[2]}'

    for i in range(num_ex):
      if splitted[2] == 'do':
        if i+1 not in data_json['done'][lec_key][cat_name]:
          keys.append(
              InlineKeyboardButton(
                  text=i+1, callback_data=f'{data} {i+1}'))
      elif splitted[2] == 'undo':
        if i+1 in data_json['done'][lec_key][cat_name]:
          keys.append(
              InlineKeyboardButton(
                  text=i+1, callback_data=f'{data} {i+1}'))
      elif splitted[2] == 'mark':
        if i+1 not in data_json['marked'][lec_key][cat_name]:
          keys.append(
              InlineKeyboardButton(
                  text=i+1, callback_data=f'{data} {i+1}'))
      elif splitted[2] == 'unmark':
        if i+1 in data_json['marked'][lec_key][cat_name]:
          keys.append(
              InlineKeyboardButton(
                  text=i+1, callback_data=f'{data} {i+1}'))

    keys.append(BACK_KEY)
    return group_keys(keys, 3), res

  elif len(splitted) == 4 \
          and splitted[1] == 'ex' \
          and (splitted[2] == 'dist' or splitted[2] == 'undist') \
          and splitted[0] in cmds:
    return BASIS_KEYS, handle('/' + data, chat_id)

  elif len(splitted) == 5 \
          and splitted[1] == 'ex' \
          and (splitted[2] == 'do' or splitted[2] == 'undo'
               or splitted[2] == 'mark' or splitted[2] == 'unmark') \
          and splitted[0] in cmds:
    return BASIS_KEYS, handle('/' + data, chat_id)

  else:
    return BASIS_KEYS, 'Invalid Button pressed'

  return None, None
