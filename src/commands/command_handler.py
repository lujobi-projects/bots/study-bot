import json
from os import getenv
from os.path import abspath


from .exercise import info as ex_info, do, undo, summary, dist, undist, mark, unmark
from .lecture import info as lec_info
from .calendar import complete_calendar

from ..utils.message import strip_at_from_msg, key_to_cmd
from ..utils.command_info import command_info

config_file = abspath(getenv('CONFIG', '/config.json'))
data_file = abspath(getenv('DATA', '/data/data.json'))

with open(config_file) as f:
  cfg = json.load(f)

with open(data_file) as f:
  data = json.load(f)


def lect(cmd, key):
  text = ''
  lec = cfg["lectures"][key]

  text += f'*{lec["full_name"]}* \[\/{key_to_cmd(key)}\]\n'
  if len(cmd) == 0:
    text += lec_info(lec)
    return True, text
  elif cmd[0] == 'ex':
    if len(cmd) == 1:
      text += ex_info(key, lec)
      return True, text
    elif (cmd[1] == 'do' and len(cmd) == 4):
      text += do(key, lec, int(cmd[2]), int(cmd[3]))
      return True, text
    elif (cmd[1] == 'undo' and len(cmd) == 4):
      text += undo(key, lec, int(cmd[2]), int(cmd[3]))
      return True, text
    elif (cmd[1] == 'mark' and len(cmd) == 4):
      text += mark(key, lec, int(cmd[2]), int(cmd[3]))
      return True, text
    elif (cmd[1] == 'unmark' and len(cmd) == 4):
      text += unmark(key, lec, int(cmd[2]), int(cmd[3]))
      return True, text
    elif (cmd[1] == 'dist' and len(cmd) == 3):
      text += dist(key, lec, int(cmd[2]))
      return True, text
    elif (cmd[1] == 'undist' and len(cmd) == 3):
      text += undist(key, lec, int(cmd[2]))
      return True, text
    else:
      return False, ''

  else:
    return False, ''


def handle(text, chat_id):
  command = strip_at_from_msg(text.lower())

  cmd_split = command.split(' ')
  if cmd_split[-1] == '':
    cmd_split = cmd_split[:-1]

  for key in cfg['lectures'].keys():
    main_cmd = cmd_split[0][1:]
    cmd_lect = key_to_cmd(key)
    if main_cmd == cmd_lect:
      if data.get('message_id') == chat_id:
        valid_cmd, ret_msg = lect(cmd_split[1:], key)
        if valid_cmd:
          return ret_msg
      else:
        return "Your are not authorized to write to the bot from that chat"
  if main_cmd == 'ex':
    if data.get('message_id') == chat_id:
      return summary()
    else:
      return "Your are not authorized to write to the bot from that chat"
  elif main_cmd == 'info':
    return command_info(escape=True)
  elif main_cmd == 'calendar':
    if data.get('message_id') == chat_id:
      return complete_calendar()
    else:
      return "Your are not authorized to write to the bot from that chat"
  elif main_cmd == 'all':
    res = ''
    for key in cfg['lectures'].keys():
      _, t = lect([], key)
      res += t + '\n'
    return res
  elif main_cmd == 'start':
    if not data.get('message_id'):
      data['message_id'] = chat_id

      with open(data_file, 'w+') as f:
        json.dump(data, f)

      return 'Successfully registered'

    else:
      return 'This bot is already registered to another user'

  return 'invalid cmd'
