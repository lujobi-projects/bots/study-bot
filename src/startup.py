from os import getenv
from os.path import abspath, join, exists
import json

config_file = abspath(getenv('CONFIG', '/config.json'))
data_file = abspath(getenv('DATA', '/data/data.json'))


def startup():
  with open(config_file) as f:
    cfg = json.load(f)

  data = {}
  if exists(data_file):
    with open(data_file, 'r+') as f:
      data = json.load(f)

  if not data.get('done'):
    data['done'] = {}
  if not data.get('dist'):
    data['dist'] = {}
  if not data.get('marked'):
    data['marked'] = {}

  for key, item in cfg['lectures'].items():
    if not data['done'].get(key):
      data['done'][key] = {}
    if not data['dist'].get(key):
      data['dist'][key] = {}
    if not data['marked'].get(key):
      data['marked'][key] = {}
    for ex_key in item.get('exercises', []):
      if not data['done'][key].get(ex_key):
        data['done'][key][ex_key] = []
      if not data['marked'][key].get(ex_key):
        data['marked'][key][ex_key] = []
      if not data['dist'][key].get(ex_key):
        data['dist'][key][ex_key] = 0

  with open(data_file, 'w+') as f:
    json.dump(data, f)
