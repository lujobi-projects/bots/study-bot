
DAY_MAP = ['Sunday', 'Monday', 'Tuesday',
           'Wednesday', 'Thursday', 'Friday', 'Saturday']


def key_to_cmd(key):
  return key.lower().replace(' ', '_')


def strip_at_from_msg(msg):
  splitted = msg.split('@')

  if len(splitted) == 1:
    return msg
  elif len(splitted) == 2:
    rest = splitted[1].split(' ')[1:] or ['']
    assemble = [splitted[0]]
    assemble.extend(rest)
    return ' '.join(assemble)
  else:
    raise Exception


def date_to_string(date):
  if 0 <= date[0] < 7 and 0 <= date[1][0] < 24 and 0 <= date[1][1] < 24:
    return f'{DAY_MAP[date[0]]}, {date[1][0]}\.00 \- {date[1][1]}\.00'
  else:
    raise Exception
