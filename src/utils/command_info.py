import json
from os import getcwd, getenv
from os.path import abspath, join

config_file = abspath(getenv('CONFIG', '/config.json'))
cfg = json.load(open(config_file))


def command_info(escape=False):
  res = ''
  if escape:
    res += f'*Study bot configured for {cfg["meta"]["user"]} \n*'
    res += f'Exercises can be marked as done \(resp undone\) using the command: `\/\<lec\> ex do \<a\> \<b\>`\nwhere `\<lec\>` denotes the lecture command, `\\\<a\>` the category as int and `\\\<b\>` the exercise as int\. To undo a exercise replace `do` with `undo`\.'
    res += f' Furthermore `do` can be replaced with `mark` with `unmark` in order to mark an exercise\.\n'
    res += f'`\/\<lec\> ex dist \<a\>` can be used to mark the next exercise of a certain category as distributed\. \n\n'
  for key in cfg['lectures'].keys():
    cmd = key.lower().replace(' ', '_')
    if escape:
      res += '\\/'
    res += f'{cmd} - { cfg["lectures"][key]["full_name"]} \n'

  res += ("\\/" if escape else "") + f'ex - Overview of all Exercises \n'
  res += ("\\/" if escape else "") + f'info - show info \n'
  res += ("\\/" if escape else "") + f'all - show all lectures\n'
  res += ("\\/" if escape else "") + f'calendar - show the calendar\n'
  res += ("\\/" if escape else "") + \
      f'start - start and thus fix this bot to the chat this command is written in \n'

  if escape:
    return res.replace('-', '\-')
  return res


if __name__ == "__main__":
  print(command_info(), flush=True)
