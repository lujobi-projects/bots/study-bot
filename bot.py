import telepot
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from os import getenv
from os.path import abspath
import json
import time

from src.commands.command_handler import handle
from src.commands.key_handler import handle_key, BASIS_KEYS
from src.startup import startup
from src.utils.message import key_to_cmd

config_file = abspath(getenv('CONFIG', '/config.json'))
with open(config_file) as f:
  cfg = json.load(f)


def reply(msg):
  keys = BASIS_KEYS

  res = ''

  if msg.get('data'):
    chat_id = msg['message']['chat']['id']
    keys, res = handle_key(msg)

  else:
    text = msg['text']
    chat_id = msg['chat']['id']
    res = handle(text, chat_id)

  reply_markup = InlineKeyboardMarkup(inline_keyboard=keys)

  bot.sendMessage(chat_id, res, reply_markup=reply_markup, parse_mode='MarkdownV2',
                  disable_web_page_preview=True)


startup()
bot = telepot.Bot(getenv('TOKEN'))
bot.message_loop(reply)
print('I am listening ...')

while 1:
  time.sleep(10)
